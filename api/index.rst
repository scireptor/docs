.. _API:

=============
sciReptor API
=============

sciReptor offers an ADC API compliant way to search and access data in its database backend.
