.. _Introduction:

============
Introduction
============


The goal of sciReptor is to be a comprehensive solution for the processing, use and storage of single-cell AIRR-seq (scAIRR-seq) data and its integration with flow cytometry (FC) and receptor affinity (RA) data.

sciReptor was originally developed [Imkeller_2016]_ to process and analyse data from Matrix PCR [Busse_2014]_ experiments. Note that sciReptor focuses **exclusively** on single-cell data, if you need to process bulk AIRR-seq data, you will find recommendations on the AIRR-C Software WG website.

sciReptor aims to comply to the AIRR Community standards for metadata [Rubelt_2017]_, exchange formats [VanderHeiden_2018]_ and programatic data access [Christley_2020]_.

It consists out of multiple components:

-  Database backend and schema
-  Data processing pipeline
-  API to access the database via HTTP
-  Tools for data import, export and analysis

