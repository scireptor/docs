.. _Database:

==================
sciReptor Database
==================

sciReptor uses a relational database as backend to store and access
data.


sciReptor schema
================

.. image:: sciReptor-schema_ERD.svg
   :width: 100 %
   :scale: 40 %
   :alt: Entity-relationship-diagram of sciReptor schema
   :align: left

Practical database organization
===============================

It is highly recommended to use a separate instance of MariaDB that is
dedicated to sciReptor usage. In general, it is assumed that users
follow this hierarchy:

*  *Database*: One per study, which can encompass multiple *Experiments*
*  *Experiment*: A single matrix or a slice of it
*  *Run*: The raw data of an individual sequencing library

A newly created *Database* can be named at the users discretion, however
the following prefixes should not be used:

*  ``library_``: This is used internally for the reference library
   databases
*  ``scireptor_``: Used for sciReptor internal information and
   study-level metadata

For performance reasons it is further advisable, that larger studies
that contain dozend of *Experiments* should be split into multiple
*Databases*.
