.. _References:

References
==========

.. [Busse_2014] Busse CE *et al*. Single-cell based high-throughput
   sequencing of full-length immunoglobulin heavy and light chain genes.
   Eur J Immunol 44:597 (2014). `DOI: 10.1002/eji.201343917`_
.. [Christley_2020] Christley S *et al*. The ADC API: a web API for the
   programmatic query of the AIRR Data Commons. Front in Big Data (2020).
   `DOI: 10.3389/fdata.2020.00022`_
.. [Imkeller_2016] Imkeller K *et al*. sciReptor: analysis of
   single-cell level immunoglobulin repertoires. BMC Bioinformatics
   17:67 (2016). `DOI: 10.1186/s12859-016-0920-1`_
.. [Ludwig_2019] Ludwig J *et al*. High-throughput single-cell
   sequencing of paired TCRα and TCRβ genes for the direct expression-
   cloning and functional analysis of murine T-cell receptors. Eur J
   Immunol 49:1269 (2019). `DOI: 10.1002/eji.201848030`_
.. [Murugan_2015] Murugan R *et al*. Direct high-throughput
   amplification and sequencing of immunoglobulin genes from single
   human B cells. Eur J Immunol 45:2698 (2015).
   `DOI: 10.1002/eji.201545526`_
.. [Rubelt_2017] Rubelt F *et al*. Adaptive Immune Receptor Repertoire
   Community recommendations for sharing immune-repertoire sequencing
   data. Nat Immunol 18:1274 (2017). `DOI: 10.1038/ni.3873`_
.. [VanderHeiden_2018] Vander Heiden JA *et al*. AIRR Community
   Standardized Representations for annotated immune repertoires.
   Front Immunol 9:2206 (2018). `DOI: 10.3389/fimmu.2018.02206`_
.. [Wahl_2021] Wahl I *et al*. *submitted*


.. ##### DOI link collection #####
.. _`DOI: 10.1002/eji.201343917`: https://doi.org/10.1002/eji.201343917
.. _`DOI: 10.1186/s12859-016-0920-1`: https://doi.org/10.1186/s12859-016-0920-1
.. _`DOI: 10.3389/fdata.2020.00022`: https://doi.org/10.3389/fdata.2020.00022
.. _`DOI: 10.1002/eji.201545526`: https://doi.org/10.1002/eji.201545526
.. _`DOI: 10.1038/ni.3873`: https://doi.org/10.1038/ni.3873
.. _`DOI: 10.3389/fimmu.2018.02206`: https://doi.org/10.3389/fimmu.2018.02206
.. _`DOI: 10.1002/eji.201848030`: https://doi.org/10.1002/eji.201848030
