Prerequisites
=============

-  Software (required)

   -  Linux system (tested: CentOS 7)
   -  MariaDB database
   -  BLAST
   -  IgBLAST
   -  Muscle
   -  RazerS
   -  Perl with BioPerl
   -  R with BioConductor (flowcore package)

-  Software (recommended for pre-processing)

   -  PandaSeq

- Configuration

   -  MariaDB user account with complete database access
   -  `.my.cnf` file holding the credentials


Basic Setup for a project
=========================

It is recommended to keep all data for a specific project within one
directory and one corresponding database scheme. Each project directory
contains its individual version of the sciReptor code, this was done
to make projects independent from each other in terms of code updates.
To implement an easy way for updates it is recommended to clone the
current version of the sciReptor git repository instead of just copying
the script files. It is shown below how to do this.

The `config` file contains all information that is required for data
processing. It does **not** contain any metadata, although some fields
(e.g. `species`) will overlap with the metadata files.


*  Database: The pipeline requires a *Database*, i.e., an instance of
   the sciReptor database schema [link database chapter] provided by a
   MariaDB 5.5 instance. The user running the processing needs full
   access to this *Database*.

*  Library: The reference library to be used by Blast and RazorS
