Data pre-processing
===================

sciReptor pipeline expects to ingest reads that cover the complete amplicon. However, Illumina as the most frequently used NGS platform cannot deliver reads that would provide full-length coverage of Ig/TCR V regions. Illumina MiSeq 2x300 bp covers this physically, but the pair-reads need to be assembled first, before they can be used by the pipeline. These pre-processing step are described below.

Requirements
------------

The preprocessing described below assumes that the following tools are install on your computer and are in the ``PATH``.

-  pandaseq
-  bbmap
-  ...

It further assumes that the sequence data has undergone basic tests for integrity and experimental quality controls (e.g., with ``fastqc``). The median quality for the first read of a read pair should be above Q20 for the entire length, for the second read it should not drop below Q20 before 250 bp.

Paired-read assembly
--------------------

.. csv-table:: PandaSeq settings (``NULL`` indicates that this option is **not** set in the commandline)
   :file: pandaseq_settings.csv
   :header-rows: 1


