.. _Pipeline:

==================
sciReptor Pipeline
==================

The sciReptor data processing pipeline is the central tool to add
previously unprocessed data into the backend database. It performs
tag identification, consensus building, VDJ annotation and integration
of FC data. Furthermore it produces quality control (QC) reports that
can be used to spot potential problems with the data early on.

.. toctree::
   :maxdepth: 1
   :caption: Topics

   installation
   preprocessing
   processing
