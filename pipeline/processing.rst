Data processing
===============

-  Read numbers should be normalized to cell numbers and tag
   identification efficiency.
-  Performing test processing runs with approx. 500 reads/cell is a
   quick way to gauge the efficiency of the experiment.
