==============
sciReptor Docs
==============

Copyright 2015-2023 by Katharina Imkeller, Christian Busse and
Francisco Arcila

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit 
https://creativecommons.org/licenses/by-sa/4.0/.
