==============
sciReptor Docs
==============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction/index
   database/index
   pipeline/index
   tools/index
   api/index

.. toctree::
   :maxdepth: 1
   :caption: Appendix:

   appendix/references

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
